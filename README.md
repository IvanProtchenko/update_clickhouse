# update_clickhouse

https://clickhouse.tech/docs/ru/operations/table_engines/mergetree/#table_engine-mergetree-ttl

остановка заббикс

1. создание db
```
CREATE DATABASE zbx ENGINE = Ordinary
```
2. создание таблиц
```
CREATE TABLE zbx.history (`day` Date, `itemid` UInt64, `clock` DateTime, `ns` UInt32, `value` Int64, `value_dbl` Float64, `value_str` String) ENGINE = MergeTree() PARTITION BY toDate(clock) ORDER BY (itemid, clock) TTL toDate(clock) + toIntervalDay(30) SETTINGS index_granularity = 8192, merge_with_ttl_timeout = 86400
```
```
CREATE TABLE zbx.history_buffer (`day` Date, `itemid` UInt64, `clock` DateTime, `ns` UInt32, `value` Int64, `value_dbl` Float64, `value_str` String) ENGINE = Buffer(zbx, history, 8, 30, 60, 9000, 60000, 256000, 256000000)
```
если нужны тренды
```
CREATE MATERIALIZED VIEW zbx.trends (`clock` DateTime('Europe/Moscow'), `itemid` UInt64, `num` AggregateFunction(count, Float64), `value_min` AggregateFunction(min, Float64), `value_avg` AggregateFunction(avg, Float64), `value_max` AggregateFunction(max, Float64)) ENGINE = AggregatingMergeTree() PARTITION BY toDate(clock) ORDER BY (clock, itemid) SETTINGS index_granularity = 8192 POPULATE AS SELECT toStartOfHour(clock) AS clock, itemid, countState(value_dbl) AS num, minState(value_dbl) AS value_min, avgState(value_dbl) AS value_avg, maxState(value_dbl) AS value_max FROM zbx.history GROUP BY clock, itemid
```
```
CREATE MATERIALIZED VIEW zbx.trends_uint (`clock` DateTime('Europe/Moscow'), `itemid` UInt64, `num` AggregateFunction(count, Int64), `value_min` AggregateFunction(min, Int64), `value_avg` AggregateFunction(avg, Int64), `value_max` AggregateFunction(max, Int64)) ENGINE = AggregatingMergeTree() PARTITION BY toDate(clock) ORDER BY (clock, itemid) SETTINGS index_granularity = 8192 AS SELECT toStartOfHour(clock) AS clock, itemid, countState(value) AS num, minState(value) AS value_min, avgState(value) AS value_avg, maxState(value) AS value_max FROM zbx.history GROUP BY clock, itemid
```
3. копирование данных
```
INSERT INTO zbx.history SELECT * FROM zabbix.history
```
4. меняем бд в конфигах сервера и фронтенда
5. запускаем заббикс

немного полезных запросов

посмотреть партиции
```
SELECT partition,database,table,name,active,bytes FROM system.parts where table='history' and database='zbx';
```
посмотреть размер бд
```
SELECT table, round(sum(bytes) / 1024/1024/1024, 2) as size_gb FROM system.parts WHERE active and database='zbx' GROUP BY table ORDER BY size_gb DESC;
```
удалить партицию
```
ALTER TABLE zbx.history DROP PARTITION '2020-04-01';
```
изменить TTL таблицы
```
ALTER TABLE zbx.history MODIFY TTL <ключ партиционирования> + INTERVAL 7 DAY;
```
```
ALTER TABLE zbx.history MATERIALIZE TTL <ключ партиционирования> + INTERVAL 7 DAY;
```